<footer>
	<div class="container-fluid">
		<div class="menu-uls">
		<div class="row all">
			<div class="col-sm-2 qr">
		    	<img src="img/qrcode.png" width="140" height="140" alt=""/> 
			</div>
			<div class="col-sm-2"> 
				<h3 class="footer_h3">2 Wheeler sharing</h3>
				<ul class="footer_ul">
					<li><a href="howitworks.html">How it works</a></li>
					<li><a href="safety.html">Safety</a></li>
					<li><a href="getinsurance.html">Get insurance</a></li>
				</ul>
			</div>
			<div class="col-sm-2">
				<h3 class="footer_h3">Company</h3>
				<ul class="footer_ul">
					<li><a href="aboutus.html">About Us</a></li>
					<li><a href="team.html">Team</a></li>
					<li><a href="ourstory.html">Our Story</a></li>
				</ul>
			</div>
	
			<div class="col-sm-2">
				<h3 class="footer_h4">.</h3>
				<ul class="footer_ul">
					<li><a href="mission.html">Mission</a></li>
					<li><a href="press.html">Press</a></li>
					<li><a href="jobs.html">Jobs</a></li>
				</ul>
			</div>
			<div class="right">
    		<b>SUPPORT</b><br>
    		<p>18001234567 </p>
      		<p>support@book2wheel.com</p>
   			<p>Skype:book2wheel_support</p>
		</div>	 
		</div>
			</div>
	</div>
  <div class="wrappclearfix"></div>

<div class="clearfix icons">
  <div class="box">
   <i class="fa fa-cc-visa" ></i>
   <i class=" fa fa-cc-mastercard"></i>
   <i class="fab fa-cc-paypal"></i>
  </div>
  <div class="box">
   <a href="https://www.instagram.com/book2wheel/"> <i class="fa fa-instagram  " ></i></a> 
   <a href="https://www.facebook.com/book2wheel/"><i class="fa fa-facebook-square  "></i></a>
   <a href="https://twitter.com/book2wheel"><i class="fa fa-twitter "></i></a> 
  </div>
  <div class=" box"  >
    <p style="font-size: 14px">Sitemap   © Book2Wheel</p>
  </div>
</div>

</footer>
		</div>	
	
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!--  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
<!--   <script src="user_profile/js/bootstrap.min.js"></script> -->
   <!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
  <script src="js/bootstrap.min.js"></script>
    
   <!--  <script src="jsjquery.blockui.min.js" type="text/javascript"></script> -->

    <!-- <script src="assets/toastr/toastr.min.js" type="text/javascript"></script>   -->
     <script src="js/jquery.validate.min.js" type="text/javascript"></script>
     <script src="js/jquery.validate.js" type="text/javascript"></script>
       <script src="js/additional-methods.min.js"> </script>
     <script src="js/user_scripts.js"></script>
     <script src="js/bootstrap-datepicker.js"></script>
       <script src="js/loadingoverlay.min.js"></script> 


    <script type="text/javascript">
     // dt = new Date();
      // var d = new Date();
      // var n = d.getFullYear();
     // var year = date.getYear();
       
$('#date1').datepicker({
  autoclose:true,
  startDate:'-0d'
}).on('changeDate',function(e){
  $('#date2').datepicker('setStartDate',e.date)
});

$('#date2').datepicker({
  autoclose:true
});


       $('#date3').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-0d'
       });
        $('#date4').datepicker({
        format: 'mm/dd/yyyy',
        startDate: '-0d'
       });
  </script>


   <script src="assets/scripts/create_user.js" type="text/javascript"></script>
	  <script>
    // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        //street_number: 'short_name',
        //route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
    //   autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
        <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPvLG_-kK6zU-PvQeiJPhyAorDQ4Jd09M&callback=initMap">
    </script>


</body>
</html>