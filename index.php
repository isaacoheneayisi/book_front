

<?php include('inc/header.php');  ?>

<div class="container center">
	<div class="row">
	    <div class="col-12">
			<div class="demo" style="font-family: 'Ludicrous'; font-weight: normal; font-style: normal;">
       		<h1>The best 2-wheel sharing service</h1> 
			</div>
		</div>
	    <div class="col-12">
	    	 <form role="form"  id="search_b" method="GET" action="bikesearch.php">
	    	 	<input type="hidden" name="opera" value="searchBike">

    	    <div id="custom-search-input">
                <div class="input-group">
                    <input type="text" class="search-query form-control" placeholder="Try entering city" name="autocomplete" placeholder="Enter your address"  onFocus="geolocate()" required="required"/>
                    <span class="input-group-btn">
                        <button type="button" disabled>
                            <span class="fas fa-map-marker-alt"></span>
                        </button>
                    </span>
                </div>
            </div>            
        </div>
        <div class="col-12">
        	<div id="search-city-input">
		               <table style="margin: auto;margin-top: 10px">
		               	<tr >
		               		<td><input class="date-own form-control input-lg validate" style="width: 200px;" type="text" required="required" id="date1" name="date1" placeholder="Start Date"></td>
		               		<td> <input data-toggle="datepicker" class="date-own form-control input validate" style="width: 200px;" type="text" required="required" id="date2" name="date2" placeholder="Return Date"></td>
		               	</tr>
		               </table>
					 

					 			
				</div>
        </div>
	</div>
	<div class="ride">
			<a class="btn btn-primary btn-lg" href="#" role="button">Let's ride</a>
		</div>
</div>
<!--test -->
<?php  include ('inc/footer.php');  ?>