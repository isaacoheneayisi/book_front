$(".signupform input").focusout(function() {
    validateInput(event.target, event.target.id);
})

  function validateInput(input, inputType) {
    var inputValue = $(input).val();
    switch(inputType)
    {
        case "firstname":
        case "lastname":
            if  (inputValue != "") {
                setInputValid(input);
            }
            else {
                setInputInvalid(input);
            }
        break;
        
        case "phone":
            if (inputValue != "" || typeof inputValue === 'number') { 
                var remainder = (inputValue % 1);
                if (remainder === 0) {
                    setInputValid(input);
                }   
                else
                {
                    setInputInvalid(input);
                }      
            }
            else {
                setInputInvalid(input);
            }
        break;
  
        case "code":
        if (inputValue.length == 4 || typeof inputValue === 'number') { 
            var remainder = (inputValue % 1);
            if (remainder === 0) {
                setInputValid(input);
            }   
            else
            {
                setInputInvalid(input);
            }      
        }
        else {
            setInputInvalid(input);
        }
        break;
  
        case "email":
            if( /(.+)@(.+){2,}\.(.+){2,}/.test(inputValue)){
                setInputValid(input);
            }
            else {
                setInputInvalid(input);
            }
        break;
  
        case "birthday":
            if (inputValue != "") {
                setInputValid(input);
            }
            else {
                setInputInvalid(input);
            }
        break;
    }   
  }
  
  function setInputInvalid(input)
  {
    $(input).removeClass("is-valid");
    $(input).addClass("is-invalid");
  }
  
  function setInputValid(input)
  {
    $(input).removeClass("is-invalid");
    $(input).addClass("is-valid");
  }

  function validateDetailsForm() { 
    var inputs = $(".signupform input");
  
    if (inputs.length == 0) {
        return false;
    }
    
    for (var i = 0; i < inputs.length; i++)
    {
        if (inputs[i].classList.contains("is-valid") == false)
        {
            return false;
        }
    }   
    return true;
  }
  
  // On click of "Next" button 
  function enterDetails() {
    if (validateDetailsForm() == true)
    {
        /*left side*/
    $('#stepicon1').css('filter', 'grayscale(100%');
    $('#stepicon2').css('filter', 'grayscale(0%)');
    
    var labels = $('.formlabel');
    for (var i = 0; i < labels.length; i++)
      {
        labels[i].classList.add("text-muted");
      }
    
    var inputs = $('.signupform input');
    for (var i = 0; i < inputs.length; i++)
      {
        inputs[i].readOnly = true;
      }
    
    nextButton.classList.add("btn-secondary");
    nextButton.disabled = true;
  
    editButton.classList.remove("invisible");
      
    /*right side*/ 
    var buttons = $('.buttonsgroup button');
    for (var i = 0; i < buttons.length; i++)
      {
        buttons[i].disabled = false;
        buttons[i].classList.remove("btn-secondary");
      }
    
    var images = $('.buttonsgroup img');
    for (var i = 0; i < images.length; i++)
      {
        $(images[i]).css('filter', 'grayscale(0%)')
      }  
    }
  }
  
  //On click of "Edit button"
  function enableEditing() {
    $('#stepicon1').css('filter', 'grayscale(0%)');
    $('#stepicon2').css('filter', 'grayscale(100%');
    
    /*left side*/
    var labels = $('.formlabel');
    for (var i = 0; i < labels.length; i++)
      {
        labels[i].classList.remove("text-muted");
      }
    
    var inputs = $('.signupform input');
    for (var i = 0; i < inputs.length; i++)
      {
        inputs[i].readOnly = false;
      }
    
    nextButton.classList.remove("btn-secondary");
    nextButton.disabled = false;
  
    editButton.classList.add("invisible");
      
    /*right side*/ 
    var buttons = $('.buttonsgroup button');
    for (var i = 0; i < buttons.length; i++)
      {
        buttons[i].disabled = true;
        buttons[i].classList.add("btn-secondary");
      }
    
    var images = $('.buttonsgroup img');
    for (var i = 0; i < images.length; i++)
      {
        $(images[i]).css('filter', 'grayscale(100%)')
      } 
  }